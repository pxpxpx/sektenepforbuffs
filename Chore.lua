--[[
	TODO:
	* Write all operations to log-file
	  Conform to epgpweb-format?
	* Write own interface for storing logs/standings/etc 
	  because epgpweb sucks balls
--]]

epb = LibStub("AceAddon-3.0"):NewAddon("epb","AceConsole-3.0")
local AceGUI = LibStub("AceGUI-3.0")
local JSON = LibStub("LibJSON-1.0")

local gSuccess = false
local announceTo = "raid"
local dryRunCB = ""
local guildies, raiders = {}, {}

local eventFrame = CreateFrame("Frame", "epbGuildRosterUpdateFrame")
local guildRoster = GuildRoster()

local epValues = {
	150, -- DMT
	100, -- ONY
	100, -- WCB
	100, -- SF
	100, -- DMF
	100, -- ZF
}

local statusTextStart = "Check if anyone got their buffs dispelled etc."

local defaults = {
  global = {
    log = {
    	"SektenEPforBuffs",
    	{}
    },
  }
}


local function eventHandler(self, GUILD_ROSTER_UPDATE)
	-- this is such a fucking bad solution to the bad solution of changing ID's
	local numOnline, _ = GetNumGuildMembers()
	epb:emptyTable(guildies)

	for i = 1, numOnline do
		local name, _, _, _, _, _, _, officerNote, _, _, _, _, _ = GetGuildRosterInfo(i)
		-- remove realm-names since we can't raid x-realm in classic
		local nName = {string.gsub(name, "-(%w+)", ""), officerNote, i}
		table.insert(guildies, i, nName)
	end
	-- epb:Print("guldies are at " .. #guildies)
end
eventFrame:SetScript("OnEvent", eventHandler);
gSuccess = eventFrame:RegisterEvent("GUILD_ROSTER_UPDATE")


function epb:OnInitialize()
	epb:RegisterChatCommand("epb", "commands")  
	self.db = LibStub("AceDB-3.0"):New("SEPFB_LOG", defaults, true)
	if self.db.global.log then
		logTable = self.db.global.log
	end
end

function epb:OnEnable()
	epb:Print("EPB ENABLED")
	epb:Print("/epb to show window")
end

function epb:OnDisable()
end

function epb:commands(msg)
	local inRaid = UnitInRaid("player");
	
	if msg == "log" then
		epb:showLog()
	elseif inRaid == nil then
		epb:Print("Error: Not in a raid!")
		return false
	elseif CanEditOfficerNote() == false then
		epb:Print("Error: You cannot write to officer notes!")
		return false
	else
		if msg == "debug" then
			epb:Print("Dumping Guildies")
			epb:Print(epb:dump(guildies))
			epb:Print("Dumping Raiders")
			epb:Print(epb:dump(raiders))
			return true
		end
		if (epbFrame == nil) then
			epb:matchRaidToGuild()
		else 
			return false
		end
	end
end

function epb:dump(o)
   if type(o) == 'table' then
      local s = '{ '
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
         s = s .. '['..k..'] = ' .. epb:dump(v) .. ', '
      end
      return s .. '} '
   else
      return tostring(o)
   end
end

function epb:tableContains(table, element)
	for index, value in pairs(table) do
		if value == element then
			return index
		end
	end
	return false
end

function epb:tablelength(t)
	local count = 0
	for _ in pairs(t) do count = count + 1 end
	return count
end

function epb:match(string,pattern)
    local string = string:match(pattern)
    if string then
       return true
    else
       return false
    end
end

-- Returns table with name, officerNote, guildId
function epb:getGuildInfoFromName(name)
	if (gSuccess == true) then
		for i = 1, #guildies do
			if (guildies[i][1] == name) then
				return guildies[i]
			end
		end
	else
		epb:Print("Error! gSuccess is not TRUE")
	end
end

function epb:emptyTable(t) 
	local count = #t
	for i=0, count do t[i]=nil end
end


function epb:matchRaidToGuild()
	if (#raiders > 0) then
		epb:Print("Emptying raiders table")
		epb:emptyTable(raiders)
	end

	-- epb:Print()

	-- DEBUG
	-- epb:Print("Matching current raid members with guild to get indices")

	if gSuccess == true then
		--[[
		 	Adds raid members to table: raiders
		 	Main table to do work on/in like adding buffs and calculating EP
		--]]
		local numMembers = GetNumGroupMembers();

		for i=1, numMembers do
			local pname = GetRaidRosterInfo(i);

			local raiderGuildInfo = epb:getGuildInfoFromName(pname)
			
			if (raiderGuildInfo ~= nil) then 	-- checks so the player is in the guild etc. 
												-- Instructions on epgpweb on how to handle players from out of guild
				local name = raiderGuildInfo[o1]
				local officerNote = raiderGuildInfo[2]
				local gid = raiderGuildInfo[3]
				
				if epb:match(officerNote,'^(%d+,%d+)$') == true then
					-- update raider table
					raiders[#raiders+1] = {["name"] = pname, ["main"] = pname, ["gid"] = gid, ["officerNote"] = officerNote, ["buffs"] = {0, 0, 0, 0, 0, 0}, ["note"] = ""}
				else 
					epb:Print(pname .. " gid:" .. gid .. " is an alt!")
					local altName = pname
					local raiderGuildInfo = epb:getGuildInfoFromName(officerNote)
					local mainName = raiderGuildInfo[1]
					local officerNote = raiderGuildInfo[2]
					local gid = raiderGuildInfo[3]
					-- update raider table
					if altName == nil then
						altName = mainName
					end 
					raiders[#raiders+1] = {["name"] = pname, ["main"] = mainName, ["gid"] = gid, ["officerNote"] = officerNote, ["buffs"] = {0, 0, 0, 0, 0, 0}, ["note"] = ""}
				end
			end
		end

		-- epb:Print(epb:dump(raiders))

		-- check for buffs
		epb:getBuffs(raiders)
	end
end

function epb:getBuffs(t)
	--[[
	The raider table
	{ 
		[1] = { 
			["ep"] = 8400,
			["buffs"] = { 
				["ony"] = 0,
				["wcb"] = 1,
				["dmt"] = 1,
				["dmf"] = 0,
			} ,
			["name"] = Glitterapa,
			["main"] = ,
			["gid"] = 3,
		} ,
		[2] = { 
			["ep"] = 7200,
			["buffs"] = { 
				-- can be filled with whatever but ony, wcb, dmt & dmf should do?
			} ,
			["name"] = Conan,
			["main"] = Rehoward,
			["gid"] = 179, -- gid of Rehoward
		} ,
	}
	--]]
	for i=1, epb:tablelength(t) do
		local pname = t[i].name
		local raiderId = i
		-- DEBUG
		--[[if pname == nil then
			epb:Print("pname är Nil? i:" .. i)
			epb:Print(t[i].gid,t[i].main)
		end--]]
		-- epb:Print("Checking raider #" .. raiderId .. " " .. pname)
		for j=1, 40 do
			-- epb:Print(pname .. " pName ")

			local buffName, _, _, _, fullTime, btime, _, _, _, spellId = UnitBuff(pname, j)
--[[			if btime ~= nil then 
				local bRemain = btime - GetTime();
				local bRemainPercent = (bRemain / fullTime) * 100
				epb:Print("Time: " .. GetTime())
				epb:Print(i .. " " .. buffName .. " btime: " .. btime .. " fTime: " .. fullTime) 
				--epb:Print("Time " .. GetTime())
				epb:Print("=" .. bRemainPercent .. "%")
			end--]]

			if (spellId == 22820 or spellId == 22818 or spellId == 22817) then -- Dire Maul
				if btime ~= nil then
					local bRemain = btime - GetTime();
					local bRemainPercent = (bRemain / fullTime) * 100
					-- epb:Print("DMT: " .. bRemainPercent)
					if bRemainPercent > 60 then 
						t[raiderId].buffs[1] = 1
					else 
						t[raiderId].note = "DMT "
						-- epb:Print(t[i].name .. " has less than 60% left on " .. buffName)
					end
				end
		  	end
			if (spellId == 22888) then -- Onyxia
				if btime ~= nil then
					local bRemain = btime - GetTime();
					local bRemainPercent = (bRemain / fullTime) * 100
					-- epb:Print("Ony: " .. bRemainPercent)
					if bRemainPercent > 60 then 
						t[raiderId].buffs[2] = 1
					else 
						t[raiderId].note = t[raiderId].note .. "0nyxia "
						-- epb:Print(t[i].name .. " has less than 60% left on " .. buffName)
					end
				end
			end
			if (spellId == 16609) then
				if btime ~= nil then
					local bRemain = btime - GetTime();
					local bRemainPercent = (bRemain / fullTime) * 100
					-- epb:Print("WCB: " .. bRemainPercent)
					if bRemainPercent > 60 then 
						t[raiderId].buffs[3] = 1
					else 
						t[raiderId].note = t[raiderId].note .. "Warchiefs "
						-- epb:Print(t[i].name .. " has less than 60% left on " .. buffName)
					end
				end
		  	end
		  	if (spellId == 23736 or spellId == 23767 or spellId == 23768 or spellId == 23766 or spellId == 23769 or spellId == 23738 or spellId == 23767 or spellId == 23765) then
		  		if btime ~= nil then
					local bRemain = btime - GetTime();
					local bRemainPercent = (bRemain / fullTime) * 100
					-- epb:Print("Ony: " .. bRemainPercent)
					if bRemainPercent > 60 then 
						t[raiderId].buffs[4] = 1
					else 
						t[raiderId].note = t[raiderId].note .. "Darkmoon Faire "
						-- epb:Print(t[i].name .. " has less than 60% left on " .. buffName)
					end
				end
		  	end
		  	if (spellId == 15366) then
		  		if btime ~= nil then
					local bRemain = btime - GetTime();
					local bRemainPercent = (bRemain / fullTime) * 100
					-- epb:Print("Ony: " .. bRemainPercent)
					if bRemainPercent > 60 then 
						t[raiderId].buffs[5] = 1
					else 
						t[raiderId].note = t[raiderId].note .. "Songflower "
						-- epb:Print(t[i].name .. " has less than 60% left on " .. buffName)
					end
				end
		  	end
		  	if (spellId == 24425) then -- Sprit of Zandalar
		  		if btime ~= nil then
					local bRemain = btime - GetTime();
					local bRemainPercent = (bRemain / fullTime) * 100
					epb:Print("Ony: " .. bRemainPercent)
					if bRemainPercent > 60 then 
						t[raiderId].buffs[6] = 1
					else 
						t[raiderId].note = t[raiderId].note .. "Sprit of Zandalar "
						epb:Print(t[i].name .. " has less than 60% left on " .. buffName)
					end
				end
		  	end
		end
	end

	-- DEBUG
	-- epb:Print(t[1].buffs[1])
	-- epb:Print("Dumping raider table", epb:dump(t))
	epb:showFrame(t)
end

function epb:showFrame(raiders)
	local raiders = raiders
	-- apparently easiest to do this with xml-templates 
	epbFrame = AceGUI:Create("Frame")
	epbFrame:SetLayout("Flow")
	epbFrame:SetTitle("Sekten EP for Buffs")
	epbFrame:SetStatusText(statusTextStart)
	epbFrame:SetCallback("OnClose", function(widget) 
		annDd:SetValue("raid")
		epb:emptyTable(guildies)
		epb:emptyTable(raiders)
		AceGUI:Release(widget) 
		epbFrame = nil
	end)
	-- scroll that will contain all the raiders and the checkboxes and perhaps a cat or two
	scrollcontainer = AceGUI:Create("SimpleGroup") -- "InlineGroup" is also good
	scrollcontainer:SetFullWidth(true)
	scrollcontainer:SetFullHeight(true) -- probably?
	scrollcontainer:SetLayout("Fill") -- important!

	epbFrame:AddChild(scrollcontainer)

	scroll = AceGUI:Create("ScrollFrame")
	scroll:SetLayout("Flow") -- Only flow works?
	scrollcontainer:AddChild(scroll)

	local header = AceGUI:Create("SimpleGroup")
	header:SetFullWidth(true)
	header:SetHeight(60)
	header:SetLayout("Flow")

	scroll:AddChild(header)

	local hname = AceGUI:Create("InteractiveLabel")
	hname:SetRelativeWidth(0.25)
	hname:SetText("Name")
	hname:SetFullHeight(true)
	hname:SetColor(1,1,0)
	header:AddChild(hname)

	local hdmt = AceGUI:Create("Label")
	hdmt:SetRelativeWidth(0.125)
	hdmt:SetText("Dire Maul")
	hdmt:SetColor(1,1,0)
	header:AddChild(hdmt)

	local hony = AceGUI:Create("Label")
	hony:SetRelativeWidth(0.125)
	hony:SetText("Onyxia")
	hony:SetColor(1,1,0)
	header:AddChild(hony)

	local hwcb = AceGUI:Create("Label")
	hwcb:SetRelativeWidth(0.125)
	hwcb:SetText("Warchiefs")
	hwcb:SetColor(1,1,0)
	header:AddChild(hwcb)

	local hsf = AceGUI:Create("Label")
	hsf:SetRelativeWidth(0.125)
	hsf:SetText("Songflower")
	hsf:SetColor(1,1,0)
	header:AddChild(hsf)

	local hwcf = AceGUI:Create("Label")
	hwcf:SetRelativeWidth(0.125)
	hwcf:SetText("Darkmoon")
	hwcf:SetColor(1,1,0)
	header:AddChild(hwcf)

	local hzg = AceGUI:Create("Label")
	hzg:SetRelativeWidth(0.125)
	hzg:SetText("Zul'Gurub")
	hzg:SetColor(1,1,0)
	header:AddChild(hzg)

	-- here comes Johnny!
	local rowFrames = {}

	for i=1, epb:tablelength(raiders) do
		local row = AceGUI:Create("SimpleGroup")
		row:SetFullWidth(true)
		row:SetLayout("Flow")

		row.cells = {}
		for j=0,6 do
			if (j == 0) then
				local cell = AceGUI:Create("Label")
				cell:SetRelativeWidth(0.25)

				local altName = ""
				if (raiders[i].name ~= raiders[i].main) then
					cell:SetText(raiders[i].name .. " (" .. raiders[i].main .. ")")
				else
					cell:SetText(raiders[i].name)
				end

				row:AddChild(cell)
			else 
				if (raiders[i].buffs[j] == 1) then 
					local cb = AceGUI:Create("CheckBox")
					cb:SetValue(true)
					cb:SetRelativeWidth(0.125)
					cb:SetUserData(i,j)
					cb:SetLabel("") -- this is apparently needed because without it the 
									-- checkboxes get weird labels every now and then
					cb:SetDescription("")
					cb:SetCallback("OnValueChanged", function (self) 
						epb:cbChange(self)
					end)
					row:AddChild(cb)
				else
					local cb = AceGUI:Create("CheckBox")
					cb:SetValue(false)
					cb:SetRelativeWidth(0.125)
					cb:SetUserData(i,j)
					cb:SetLabel("")
					cb:SetDescription("")
					cb:SetCallback("OnValueChanged", function (self) 
						epb:cbChange(self)
					end)
					row:AddChild(cb)
				end
			end
		end
		scroll:AddChild(row)
	end

	local hControls = AceGUI:Create("Heading")
	hControls:SetText("Change EP values and write to officer notes")
	hControls:SetRelativeWidth(1)
	hControls:SetHeight(40)
	scroll:AddChild(hControls)

	local controls = AceGUI:Create("SimpleGroup")
	controls:SetFullWidth(true)
	controls:SetLayout("Flow")
	scroll:AddChild(controls)

	local dmtEp = AceGUI:Create("EditBox")
	dmtEp:SetText(epValues[1])
	dmtEp:SetLabel("DMT EP:")
	dmtEp:SetMaxLetters(4)
	dmtEp:SetRelativeWidth(0.1)
	controls:AddChild(dmtEp)

	local onyEp = AceGUI:Create("EditBox")
	onyEp:SetText(epValues[2])
	onyEp:SetLabel("ONY EP:")
	onyEp:SetMaxLetters(4)
	onyEp:SetRelativeWidth(0.1)
	controls:AddChild(onyEp)

	local wcbEp = AceGUI:Create("EditBox")
	wcbEp:SetText(epValues[3])
	wcbEp:SetLabel("WCB EP:")
	wcbEp:SetMaxLetters(4)
	wcbEp:SetRelativeWidth(0.1)
	controls:AddChild(wcbEp)

	local sfEp = AceGUI:Create("EditBox")
	sfEp:SetText(epValues[4])
	sfEp:SetLabel("SF EP:")
	sfEp:SetMaxLetters(4)
	sfEp:SetRelativeWidth(0.1)
	controls:AddChild(sfEp)

	local dmfEp = AceGUI:Create("EditBox")
	dmfEp:SetText(epValues[5])
	dmfEp:SetLabel("DMF EP:")
	dmfEp:SetMaxLetters(4)
	dmfEp:SetRelativeWidth(0.1)
	controls:AddChild(dmfEp)

	local zfEp = AceGUI:Create("EditBox")
	zfEp:SetText(epValues[6])
	zfEp:SetLabel("ZG EP:")
	zfEp:SetMaxLetters(4)
	zfEp:SetRelativeWidth(0.1)
	controls:AddChild(zfEp)

	local submit = AceGUI:Create("Button")
	submit:SetText("Write to OfficerNotes")
	submit:SetCallback("OnEnter", function () epbFrame:SetStatusText("Now be careful young man!") end)
	submit:SetCallback("OnLeave", function () epbFrame:SetStatusText(statusTextStart) end)
	submit:SetCallback("OnClick", function () 
		epb:calcEp(raiders)
		annDd:SetValue("raid")
		epb:emptyTable(guildies)
		epb:emptyTable(raiders)
		epbFrame:Hide()
	end)
--	submit:ClearAllPoints()
--	submit:SetPoint("RIGHT") 	-- y u no work?
								-- apparently needed to make a "custom layout" for the container -.-
	controls:AddChild(submit)

	local settings = AceGUI:Create("SimpleGroup")
	settings:SetFullWidth(true)
	settings:SetLayout("Flow")
	scroll:AddChild(settings)

	annDd = AceGUI:Create("Dropdown")
	annDd:SetLabel("Annouce Ep changes to:")
	annDd:SetRelativeWidth(0.4)
	annDd:SetList({
		["raid"] 		=	"Raid Channel",
  		["whisper"] 	=	"Whisper",
  		["guild"]		=	"Guild Channel",
  		["noannounce"] 	=	"Don't announce changes"
	},
	{
		-- set order of items in Dropdown
		"raid", "whisper", "guild", "noannounce"
	})
	annDd:SetValue("raid")
	annDd:SetCallback("OnValueChanged", function(self)
  		announceTo = self.value
  		epb:Print(self.value)
	end)
	settings:AddChild(annDd)

	dryRunCB = AceGUI:Create("CheckBox")
	dryRunCB:SetValue(false)
    dryRunCB:SetRelativeWidth(0.3)
	dryRunCB:SetLabel("Dry run")
	dryRunCB:SetFullWidth(false)
--	dryRunCB:SetDescription("Performs a buffcheck and writes out the values in chat")
	settings:AddChild(dryRunCB)

	logButton = AceGUI:Create("Button")
	logButton:SetText("Export Log")
	logButton:SetCallback("OnEnter", function () epbFrame:SetStatusText("Show log as JSON for export") end)
	logButton:SetCallback("OnLeave", function () epbFrame:SetStatusText(statusTextStart) end)
	logButton:SetCallback("OnClick", function () 
		epb:showLog()
		epbFrame:Hide()
	end)
	settings:AddChild(logButton)	
end

function epb:dumpRaiders(t)
	for i=1, #t do
		local string = t[i].name .. " " .. " EP: " .. t[i].officerNote .. " guildId: " .. t[i].gid .. "\n          Buffs: " .. t[i].buffs[1] .. ", " .. t[i].buffs[2] .. ", " .. t[i].buffs[3] .. ", " .. t[i].buffs[4]
		epb:Print(string)
	end
end

function epb:calcEp(raiders)
	for i=1, #raiders do
		local currentEp = string.gsub(raiders[i].officerNote, ",(%d+)$", "")
		local raiderGp = string.gsub(raiders[i].officerNote, "^(%d+),", "")
		local newEp = currentEp
		local buffEp = 0
		local buffStr = ""
		local j = 1
		for k,v in pairs(raiders[i].buffs) do
			if raiders[i].buffs[j] == 1 then
				newEp = newEp + epValues[j]
				buffEp = epValues[j]
				buffStr = buffStr .. "+" .. epValues[j]
				-- DEBUG
				-- epb:Print(raiders[i].name .. " " .. currentEp .. " (" .. buffStr .. ")")
			end
			j = j + 1
		end
		-- DEBUG
		-- epb:Print(raiders[i].name .. " From: " .. currentEp .. "EP to: " .. newEp .. "(" .. currentEp .. "" .. buffStr .. ")")
		-- epb:Print("New officerNote => " .. newEp .. "," .. raiderGp)
		-- oepb:Print("$newEp " .. type(newEp) .. " " .. newEp)
		if (tonumber(newEp) > tonumber(currentEp + 400)) then
			newEp = currentEp + 400
		end

		local newEPGP = newEp .. "," .. raiderGp
		-- DEBUG
		-- epb:Print(raiders[i].name .. " (" .. raiders[i].main .. ")" .. ":" .. raiders[i].gid .. " => " .. newEPGP)
		local dryRunToggle = dryRunCB:GetValue()
		if (dryRunToggle == true) then
			local mainName = raiders[i].main
			local ggifn = epb:getGuildInfoFromName(mainName)
			epb:Print(raiders[i].name .. " (" .. raiders[i].main .. ")" .. " Fetched gId:" .. raiders[i].gid .. " (Newly Fetched gId: " .. ggifn[3] .. ") => " .. newEPGP .. " NOTE: " .. raiders[i].note )
			-- logTable[#logTable + 1] = {["name"] = raiders[i].name, ["main"] = raiders[i].main, ["gid"] = raiders[i].gid, ["newEp"] = newEp}
			-- epbFrame:Hide()
		else 
			-- the scary stuff
			GuildRosterSetOfficerNote(raiders[i].gid, newEPGP)
			if (tonumber(newEp) > tonumber(currentEp)) then
				logTable[#logTable + 1] = {["timestamp"] = time(), ["name"] = raiders[i].name, ["main"] = raiders[i].main, ["gid"] = raiders[i].gid, ["currentEp"] = currentEp, ["newEp"] = newEp, ["note"] = raiders[i].note}
			end

			-- check so we got everything right?
			-- we can't do that due to the roster only updating every 10 seconds
			-- if (epb:checkEpGpChange(raiders[i].gid, raiders[i].main, newEPGP) == true) then
			epb:announceEp(raiders[i].name, raiders[i].main, currentEp, newEp, raiders[i].note)
			-- end
			-- epbFrame:Release()
		end
	end
end

function epb:checkEpGpChange(gid, name, newEPGP)
	-- sigh
	epb:Print("checkEpGpChange")
	local _, _, _, _, _, _, _, officerNote, _, _, _, _, _ = GetGuildRosterInfo(gid)
	if (officerNote ~= newEPGP) then
		epb:Print("ERROR: The newEPGP value (" .. newEPGP .. ") for " .. name .. " is NOT OK! OfficerNote = " .. officerNote)
		return false
	else
		return true
	end
end


function epb:announceEp(name, main, oldEp, newEp, note)
	local epDifference = newEp - oldEp
	-- epb:Print(epDifference .. oldEp .. " " .. newEp)
	
	if (epDifference > 0) then -- no need to announce noChange?
		-- epb:Print(message)
		if (announceTo == "whisper" and dryRunCB ~= "") then
			local message = main .. " has been awarded " .. epDifference .. " EP for your buffs! Your new EP is " .. newEp .. " (was " .. oldEp .. ")"
			if string.len(note) > 0 then
				message = message .. " (Buffs under 60%: " .. note .. ")"
			end
			SendChatMessage(message,"WHISPER" , nil, name);
		elseif (announceTo == "raid" and dryRunCB ~= "") then
			local message = main .. " has been awarded " .. epDifference .. " EP for buffs! New EP is " .. newEp .. " (was " .. oldEp .. ")"
			if string.len(note) > 0 then
				message = message .. " (Buffs under 60%: " .. note .. ")"
			end
			SendChatMessage(message, "RAID" , nil, name);
		elseif (announceTo == "guild" and dryRunCB ~= "") then
			local message = main .. " has been awarded " .. epDifference .. " EP for buffs! New EP is " .. newEp .. " (was " .. oldEp .. ")"
			if string.len(note) > 0 then
				message = message .. " (Buffs under 60%: " .. note .. ")"
			end
			SendChatMessage(message, "GUILD" , nil, name);
		elseif (announceTo == "noannounce") then
			return true
		end
	end
end

function epb:cbChange(self)
	local raiderNo,raiderBuff = _,_

	for k,v in pairs(self.userdata) do
		raiderNo = k
		raiderBuff = v
	end
	
	if self.checked == true then
		-- DEBUG
		-- epb:Print("Raider " .. raiders[raiderNo].name .. " buff#: " .. raiderBuff .. " is checked")
		raiders[raiderNo].buffs[raiderBuff] = 1
	else 
		-- DEBUG
		-- epb:Print("Raider " .. raiders[raiderNo].name .. " buff#: " .. raiderBuff .. " is unchecked")
		raiders[raiderNo].buffs[raiderBuff] = 0
	end
	-- DEBUG
	-- epb:dumpRaiders(raiders)
end

function epb:showLog()
	--[[
	[
		"SektenEPforBuffs",
		{
			"name":"Oxascand",
			"gid":138,
			"newEp":3800,
			"timestamp":1587473477,
			"currentEp":"3650",
			"main":"Oxascand"
		},
		...
	]
	--]]
	epbLogFrame = AceGUI:Create("Frame")
	epbLogFrame:SetLayout("Flow")
	epbLogFrame:SetTitle("Personal logs")
	epbLogFrame:SetStatusText("1) Select all. 2) Copy and Paste the JSON to https://epgp.sekten.eu")
	epbLogFrame:SetCallback("OnClose", function(widget) 
		AceGUI:Release(widget) 
		epbLogFrame = nil
	end)

	logContent = AceGUI:Create("MultiLineEditBox")
	logContent:SetFocus()
	logContent:HighlightText(0,-1)
	logContent:SetText(JSON.Serialize(logTable))
	logContent:SetFullWidth(true)
	logContent:SetFullHeight(true)
	epbLogFrame:AddChild(logContent)
end